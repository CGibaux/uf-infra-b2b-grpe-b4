# Pour lancer mes conteneurs : 
```docker stack deploy projet-uf-infra --compose-file docker-compose.yml```

# Utilisation de Docker Swarm (deploy replicas)
```
docker swarm init
docker stack deploy projet-uf-infra --compose-file docker-compose.yml
```

# La commande openssl pour les certificats SSL
```openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout hostname.key -out hostname.crt```

# Comment créer un vhost avec nginx-proxy
## En utilisant la variable d'environnement VIRTUAL_HOST sur les conteneurs cibles des vhosts

# Fonctionnement de xip.io
## Avoir une résolution en local

# Expliquer le xip.io (Voir Annexe)
## le xip.io est un nom de domaine qui permet de faire tourner mes applications en local. exemple : wordpress.127.0.0.1.xip.io = nomDuService.IPpublique.nomDeDomaine
## N'ayant pas de nom de domaine publique (.com, .fr, .org) j'ai donc utilisé le .xip.io afin de résoudre mon ip local sur un nom de domaine ainsi permettant à mon reverse proxy de gérer des vhosts. 

# Expliquer le Docker Swarm
## Docker Swarm est une des nouveautées venu avec la version 1.12 de Docker Engine. Docker Swarm permet d'orchestrer tout nos conteneurs et je vais vous l'expliquer.

## Swarm est un orchestrateur, son principe consiste à avoir de mutliples hôtes docker qui tournent en mode "Cluster" et qui agissent comme des "managers" (qui gèrent les membres et délègue) et des "workers" (qui gèrent les workloads). Un hôte Docker que l'on choisi peut être soit un "manager" soit un "worker" ou soit effectuer les deux rôles. Quand on créer un service, on défini son état désiré (nombre de replicas, de network et de ressources stockages disponible aux ports qui vont être exposés et plus encore). Docker Swarm travail à faire en sorte que l'état désiré soit appliqué en permanence. Par exemple, si un "node" worker tombe en panne, Docker Swarm planifie les tâches attribué au "node" en panne aux autres "nodes". une "tâche" est un conteneur en marche qui fait partie d'un service orchestré par le "Swarm Manager".

# Problèmes rencontrés
## base de données redondées , fonctionnement du reverse-proxy, https (problème majeur), problèmes d'authentification sur le wordpress

# 1 - Problème : Fonctionnement du reverse-proxy (Nginx)
##  - Solution : Le reverse-proxy nous sert à faire tourner nos applications sur un seul et même port, le port 80 pour le http et le port 443 pour le https. Dans notre cas nous utilisons le port 443. Le plus gros problème était de faire passer nos applications par le load balancer afin de ne pas surcharger les serveurs. Nous avons donc utilisés Nginx qui nous sert à la fois de reverse-proxy et de load balancer et qui nous permet de réduire le nombre de programmes utilisés dans ce projet afin d'optimiser la charge serveur et éviter les conflits.

# 2 - Problème : Https
##  - Solution : Une fois nos applications créer, nous devons nous occuper de l'aspect sécurité de notre programme. Pour cela nous avons utilisé le protocol TSL (Transport Layer Security). Pour ce faire, nous avons créer un certificat auto-signé. Ce certificat est authentique mais n'est pas validé car ce n'est pas reconnu par les entreprises qui valident les certificats https.

# 3 - Problème : Base de données redondés
##  - Solution : Faire répliquer des base de données est un problème car ce n'est pas aussi facile que de répliquer une application. Quand une application crash, une autre peut prendre la relève sans trop de soucis. Mais quand il sagit d'une base de données ce n'est pas la même histoire car on ne peut pas simplement "remplacer" la base car sinon, les données se perdent. Pour résoudre le problème, nous avons utilisés les images Bitnami (voir Annexe) et grâce à ça, nous avons pu faire redonder nos BDD sans perte de données.

# 4 - Problème : Problèmes d'authentification sur le Wordpress
##  - Solution : Nous avons rencontrés un problème d'authentification sur la base de données du Wordpress. l'erreur était la suivante : ```php mysqli_connect: authentication method unknown to the client [caching_sha2_password]```. Cette erreur était plus longue à comprendre que à résoudre car il fallait deviner que notre version de php (latest) était obsolète et qu'il fallait retrograder sur la version PHP7.4 (Voir Annexe)


# Annexe (Sources)
## xip.io (http://xip.io)
## MongoDB (https://docs.mongodb.com/manual/replication/)
## Base de données redondées (https://hub.docker.com/r/bitnami/mysql/, https://hub.docker.com/r/bitnami/mongodb)
## Fonctionnement du reverse-proxy (https://hub.docker.com/r/jwilder/nginx-proxy)
## https (https://wiki.openssl.org/index.php/Command_Line_Utilities)
## Problèmes d'authentification sur le wordpress (https://stackoverflow.com/questions/50026939/php-mysqli-connect-authentication-method-unknown-to-the-client-caching-sha2-pa)


# Portainer une interface graphique pour les gouverner tous !
## Afin de gérer nos stacks d'une manière claire et précise, nous avons utilisés Portainer qui est une interface graphique de gestion de stacks avec Docker.